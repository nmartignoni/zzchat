<?php

$LANG = array(

'L_LANGUE'						=> 'Français',

'L_LOGIN'						=> 'Identifiant',
'L_PASSWORD'					=> 'Mot de passe',
'L_INSCRIPTION'					=> 'Inscription',
'L_CHAT'						=> 'Chat',
'L_FREE_CHAT'					=> 'Espace libre',
'L_LOG_IN'						=> 'Connexion',
'L_LOG_OUT'						=> 'Deconnexion',
'L_SIGN_UP'						=> 'S\'enregistrer',

'L_SEND'						=> 'Envoyer',

'L_NO_USER'						=> 'Aucun utilisateur est connecté',
'L_ME'							=> 'Moi',

'L_INTRO'						=> 'Bienvenue sur le super génial ZZChat',
'L_INTRO_TEXT'					=> 'Projet développé par Noël Martignoni dans le cadre du cours de développement web',

'L_ERR_PAGE_NOT_FOUND'			=> 'Page non trouvée',

'L_ERR_EMPTY'					=> 'Champs vides',
'L_ERR_WRONG'					=> 'Identifiant ou mot de passe incorrecte',
'L_ERR_ALREADY'					=> 'Identifiant indisponible',

);
?>