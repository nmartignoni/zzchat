<?php

$LANG = array(

'L_LANGUE'						=> 'English',

'L_LOGIN'						=> 'Login',
'L_PASSWORD'					=> 'Password',
'L_INSCRIPTION'					=> 'Register',
'L_CHAT'						=> 'Chat',
'L_FREE_CHAT'					=> 'Free space',
'L_LOG_IN'						=> 'Log In',
'L_LOG_OUT'						=> 'Log Out',
'L_SIGN_UP'						=> 'Sign Up',

'L_SEND'						=> 'Send',

'L_NO_USER'						=> 'No user connected',
'L_ME'							=> 'Me',

'L_INTRO'						=> 'Welcome to the amazing ZZChat',
'L_INTRO_TEXT'					=> 'Project developed by Noël Matignoni for the web development courses',

'L_ERR_PAGE_NOT_FOUND'			=> 'Page not found',

'L_ERR_EMPTY'					=> 'Empty field',
'L_ERR_WRONG'					=> 'Invalid login or password',
'L_ERR_ALREADY'					=> 'This login is unavailable',

);
?>