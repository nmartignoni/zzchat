<?php

/**
 * zzChatShow
 *
 * @package ZZCHAT
 * @author	Noel Martignoni
 * @brief   class managing display on sdtout
 **/
class zzChatShow
{

	private static $instance = null;
	private $lang;
	private $error;
	
	private $webpagelang;
	
	private $langfile = array(
		'1' => 'core/lang/en.php',
		'2' => 'core/lang/fr.php',
		'3' => 'core/lang/ru.php'
	);
		
	private $langName = array(
		'1' => 'English',
		'2' => 'Français',
		'3' => 'Pусский'
	);

	/**
	 * Function managing singleton design pattern
	 *
	 **/
	public static function getInstance()
	{
		if (!isset(self::$instance))
			self::$instance = new zzChatShow();
		return self::$instance;
	}

	/**
	 * Constructor a zzChatShow object
	 *	
	 **/
	public function __construct($lang, $error = '-1')
	{	
		if(is_file($this->langfile[$lang]))
		{
			include($this->langfile[$lang]);
			$this->lang = $LANG;
		}
		$this->webpagelang = $lang;
		$this->error = $error;
	}
	
	/**
	 * Function use to to display a string
	 *
	 **/
	public function lang($key='')
	{
		if(isset($this->lang[$key]))
			echo $this->lang[$key];
		else
			echo $key;
	}
	
	/**
	* Create a HTML Header
	*
	*/
	public function createHeaderHTML()
	{
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"';
    	echo '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';

		echo '<html>';

  		echo '<head>';
    	echo '<title>ZZChat</title>';
    
   		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
    	echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />';
    
    	echo '<link rel="stylesheet" type="text/css" href="themes/style.css">';
    	
    	echo '<script type="text/javascript">';
    	echo 'var my_id ='.$_SESSION['user'];
    	echo '</script>';
    	
    	echo '<script type="text/javascript" src="core/lib/script.js"></script>';
  		
  		echo '</head>';

  		echo '<body>';
	}
	
	/**
	* Create a HTML footer
	*
	*/
	public function createfooterHTML()
	{
		echo '</body>';
		echo '</html>';
	}
	
	/**
	* Function use to generate a print error message
	*/
	public function printError($error)
	{
		if ($error == 1)
		{
			echo '<div id="error">';
			echo $this->lang('L_ERR_EMPTY');
			echo '</div>';
		}
		else if ($error == 2)
		{
			echo '<div id="error">';
			echo $this->lang('L_ERR_WRONG');
			echo '</div>';
		}
		else if ($error == 3)
		{
			echo '<div id="error">';
			echo $this->lang('L_ERR_ALREADY');
			echo '</div>';
		}
	}
	
	/**
	* Generate the register and login page
	*
	*/
	public function createRegisterAndLoginPage()
	{
		echo '<header>';
		
		echo '<h1>ZZCHAT</h1>';
		
		$this->createLangForm();
		
		echo '<div id="log">';
		echo '<form action="#" method="post">';
		
		echo '<label for="login">'.$this->lang('L_LOGIN').' : </label>';
		echo '<input type="text" name="login" id="login" />';
		
		echo '<label for="password">'.$this->lang('L_PASSWORD').' : </label>';
		echo '<input type="password" name="password" id="password" />';
		
		echo '<input type="submit" name="submit_log_in" value="';
		echo $this->lang('L_LOG_IN');
		echo '">';
		
		echo '</form>';
		echo '</div>';
						
		echo '</header>';
		
		echo '<div id="allpage">';
		echo '<div id="page">';	
		
		$this->printError($this->error);	
		
		echo '<menu>';
				
		echo '<h2>';
		echo $this->lang('L_INSCRIPTION');
		echo'</h2>';
			
		echo '<div id="register">';	
		echo '<form action="#" method="post">';
		
		echo '<label for="login">';
		echo $this->lang('L_LOGIN');
		echo ' : </label>';
		
		echo '<input type="text" name="login" id="login" />';
		echo '</br>';
		
		echo '<label for="password">';
		echo $this->lang('L_PASSWORD');
		echo ' : </label>';
		
		echo '<input type="password" name="password" id="password" />';
		echo '</br>';
		echo '<input type="submit" name="submit_sign_up" value="';
		echo $this->lang('L_SIGN_UP');
		echo '">';
		echo '</form>';
		echo '</div>';
		
  		echo '</menu>';
  				
		echo '<section>';		
		echo '<h3>';
		echo $this->lang('L_INTRO');
		echo '</h3>';
		echo '<p>';
		echo $this->lang('L_INTRO_TEXT');
		echo '</p>';
		
		echo '<div id="talkbubble">Hello World !</div>';
		echo '<div id="talkbubble2">Let them talk!</div>';
		
  		echo '</section>';
  		
  		echo '</div>';
  		echo '</div>';
  		
  		$this->createFooter();
	}
	
	/**
	* Generate the home page
	*
	*/
	public function createHomePage()
	{
	
		echo '<header>';
		echo '<h1>ZZCHAT</h1>';
		
		$this->createLangForm();
		
		echo '<div id="log">';
		echo '<form action="#" method="post">';
		echo '<input type="submit" name="submit_log_out" value="';
		echo $this->lang('L_LOG_OUT');
		echo '">';
		echo '</form>';
		echo '</div>';
		
		echo '</header>';
		
		echo '<div id="allpage">';
		echo '<div id="page">';
				
		
		$this->createMenuHomePage();
		
		$this->createSectionHomePage();
  		
  		echo '</div>'; /* page */
  		echo '</div>'; /* allpage */
  		
  		$this->createFooter();
	}
	
	/**
	* Generate the lang form
	*
	*/
	private function createLangForm()
	{
		echo '<div id="lang">';
		echo '<form action="#" method="post">';
		echo '<select onchange="this.form.submit()" name="submit_lang">';
		$i = 1;
		foreach($this->langName as $lang)
		{
			if ($i == $this->webpagelang)
				echo '<option selected="selected" value="'.$i.'">'.$lang.'</option>';
			else
				echo '<option value="'.$i.'">'.$lang.'</option>';
			$i++;
		}
		echo '</select>';
		echo '</form>';
		
		echo '</div>';
	}
	
	/**
	* Generate the home page section code
	*
	*/
	private function createSectionHomePage()
	{
		echo '<section>';
								
		echo '<div id="messages">';
		echo zzChatConversation::getConversation($_SESSION['user'],0);
		echo '</div>';
				
		$this->createTextForm();
				
  		echo '</section>';
	}
	
	/**
	* Create the home page menu
	*
	*/
	private function createMenuHomePage()
	{
		$user_list = zzChatUser::getOnlineUser();
		
		echo '<menu>';
		
		echo '<h2>';
		echo $this->lang('L_CHAT');
		echo'</h2>';
		
		echo '<div id="listOfUser">';
		echo zzChatUser::getListOfOnlineUserHTML($_SESSION['user'], 0);
		echo '</div>';
		
  		echo '</menu>';
	}
	
	/**
	* create the send message text form
	*/
	private function createTextForm()
	{		
		echo '<textarea id="message_text_area" rows="6" name="text_message"></textarea>';
		echo '<input class="send" onClick="sendMessage()" name="" type="submit" value="';
		echo $this->lang('L_SEND'); /*submit_send_message*/
		echo '">';
	}
	
	/**
	* create the page footer
	*/
	private function createFooter()
	{
		echo '<footer>';
  		echo 'ISIMA - Noël Martignoni ZZ2F5';
  		echo '</footer>';
	}

}
?>