<?php

/**
 * zzChatUser
 *
 * @package ZZCHAT
 * @author	Noel Martignoni
 * @brief   Class for managing users
 **/
class zzChatUser
{

	private $id;
	private $login;
	private $password;
	private $lang;

	/**
	 * Construct a new empty zzChatUser
	 *
	 * @param   int
	 * @return	null
	 **/
	public function __construct()
	{

	}
	
	/**
	*
	*/
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	*
	*/
	public function getId()
	{
		return $this->id;
	}
	
	/**
	*
	*/
	public function setLogin($login)
	{
		$this->login = $login;
	}
	
	/**
	*
	*/
	public function getLogin()
	{
		return $this->login;
	}
	
	/**
	*
	*/
	public function setPassword($password)
	{
		$this->password = $password;
	}
	
	/**
	*
	*/
	public function getPassword()
	{
		return $this->password;
	}
	
	/**
	*
	*/
	public function setLang($lang)
	{
		$this->lang = $lang;
	}
	
	/**
	*
	*/
	public function getLang()
	{
		return $this->lang;
	}
	
	/**
	* Return the online user list
	*/
	static public function getOnlineUser()
	{
		$url = 'data/online_user.xml';
		$user_list = 0;
				
		if (file_exists($url)) 
		{
			$xml = simplexml_load_file($url);
			$userlistsize = 0;
			
			foreach($xml->userid as $userid)
			{
				$userlistsize++;
			}
			
			if ($userlistsize-1 != 0)
			{
				$user_list = array($userlistsize-1);
				$i = 0;
			
				foreach($xml->userid as $userid)
				{				
					if (strcmp($userid, $_SESSION['user']) != 0)
					{
						$user = zzChatUser::getUser($userid);
						$user_list[$i] = $user;
						$i++;
					}
				}
			}
		}
				
		return $user_list;
	}
	
	/**
	* add an user to online user list when is connecting
	*
	*/
	static public function addOnlineUser($id)
	{
		$url = 'data/online_user.xml';
		$error = false;
		
		if (file_exists($url)) 
		{
    		$xml = simplexml_load_file($url);
    		
    		foreach($xml->userid as $userid)
			{				
				if (strcmp($userid, $id) == 0)
				{
					$error = true;
       	 			break;
				}
			}
    		
    		if (!$error)
    		{
    			$xml->addchild("userid",$id);
    		}
			$xml->asXml($url);
		}
	}
	
	/**
	* delete an user from user list when is deconnecting
	*
	*/
	static public function deleteOnlineUser($id)
	{
		$url = 'data/online_user.xml';
		$error = false;
		
		if (file_exists($url)) 
		{
    		$xml = simplexml_load_file($url);
    		
    		foreach($xml->userid as $userid)
			{				
				if (strcmp($userid, $id) == 0)
				{
					$dom = dom_import_simplexml($userid);
       	 			$dom->parentNode->removeChild($dom);
       	 			break;
				}
			}

			$xml->asXml($url);
		}
	}
	
	/**
	* return an zzChatUser object who is has the id $id
	*/
	static public function getUser($id)
	{
		$url = 'data/users.xml';
			
		$user_return = new zzChatUser();
		
		if (file_exists($url)) 
		{
			$xml = simplexml_load_file($url);
			
			foreach($xml->user as $user)
			{				
				if (strcmp($user->id, $id) == 0)
				{
					$user_return->setId($user->id);
					$user_return->setLogin($user->login);
					$user_return->setPassword($user->password);
					break;
				}
				echo $user_return->getLogin();
			}
		}
		return $user_return;
	}
	
	/**
	* search a user by is login
	*/
	static public function searchUser($login)
	{
		$url = 'data/users.xml';
		$userObj = new zzChatUser();
		$userObj->setId(0);
		$password = '';
		
		if (file_exists($url)) 
		{
			$xml = simplexml_load_file($url);
			
			foreach($xml->user as $user)
			{				
				if (strcmp($user->login, $login) == 0)
				{
					$userObj->setId($user->id);
					$userObj->setLogin($user->login);
					$userObj->setPassword($user->password);
					break;
				}
			}
		}
		
		return $userObj;
	}
	
	/**
	* register a new user
	*/
	static public function addUser($login, $password)
	{
		$url = 'data/users.xml';
		$user_id = 0;
		
		if (file_exists($url)) 
		{
    		$xml = simplexml_load_file($url);
    		
    		if (zzChatUser::loginIsAvailable($login)) 
    		{
    			// verification des donnees
    			
    			$user_id = zzChatUser::getNewId();
    			
    			$account =$xml->addChild("user");
    			$account->addchild("id", $user_id);
				$account->addchild("login", $login);
				$account->addChild("password", zzChatUser::codePassword($password));
    		}

			$xml->asXml($url);
		}
		
		return $user_id;
	}
	
	/**
	*  check if the login is available
	*/
	static public function loginIsAvailable($login)
	{
		$url = 'data/users.xml';
		$error = false;
		
		if (file_exists($url)) 
		{
			$xml = simplexml_load_file($url);
			
			foreach($xml->user as $user)
			{
				if (strcmp($user->login, $login) == 0)
				{
					$error = true;
					break;
				}
			}
		}
		else
		{
			$error = true;
		}
		return !$error;
	}
	
	/**
	* generate a the next id available
	*/
	static public function getNewId()
	{
		$url = 'data/users.xml';
		$new_id = 0;
		$error = false;
		
		if (file_exists($url)) 
		{
			$xml = simplexml_load_file($url);
			
			foreach($xml->user as $user)
			{
				if (intval($user->id) > $new_id)
				{
					$new_id = intval($user->id);
				}
			}
		}
		
		return $new_id+1;
	}
	
	/**
	* crypt the password with MD5
	*/
	static private function codePassword($password)
	{
		return md5($password);
	}
	
	/**
	* user identification procedure
	*/
	static public function identifyUser($login, $password)
	{
		$userid = 0;
				
		$userObj = zzChatUser::searchUser($login);
						
		if ($userObj->id != 0)
		{			
			if (strcmp($userObj->getPassword(), md5($password)) == 0)
			{
				$userid = (string)$userObj->getId();
			}
		}
		return $userid;
	}
	
	/**
	* Generate the HTML code for displaying the online user list
	*
	*/
	static public function getListOfOnlineUserHTML($current_user_id, $user_talk_to = -1)
	{
		$user_list = zzChatUser::getOnlineUser();
		
		$content = '<ul class="online">';
		
		if ($user_talk_to == 0)
			$content .= '<li class="selected"><a href="javascript:changeConversation(';
		else
			$content .= '<li><a href="javascript:changeConversation(';
			
		$content .= $current_user_id;
		$content .= ',0);">';
		$content .= 'Hello World!';
		$content .= '</a></li>';
		
		if ($user_list > 0)
		{	
			$i = 1;
			foreach ($user_list as $user)
			{
				if ($user->getId() == $user_talk_to)
					$content .= '<li class="selected"><a href="javascript:changeConversation(';
				else
					$content .= '<li><a href="javascript:changeConversation(';
					
				$content .= $current_user_id;
				$content .= ',';
				$content .= $user->getId();
				$content .= ');">';
				$content .= $user->getLogin();
				$content .= '</a></li>';
				
				$i++;
			}
		}
		$content .= '</ul>';
		
		return $content;
	}

}
?>