/**
*
*/
function getXMLHTTPRequest()
{
  	try 
  	{
    	req=new XMLHttpRequest();
  	}
  	catch(exc1)
  	{
    	try
    	{
      		req=new ActiveXObject("Msxml2.XMLHTTP");
    	}
    	catch(exc2)
    	{
      		try 
      		{
				req=new ActiveXObject("Microsoft.XMLHTTP");
      		}
      		catch(exc3)
      		{
				req=false;
      		}
    	}
  	}
  	
  	return req;
}

var my_request = getXMLHTTPRequest();
var current_conversation = 0;

/**
*
*/
function reloadData()
{
	reloadConversation(my_id, current_conversation);
	reloadListOfUserOnline(my_id, current_conversation);
	setTimeout(reloadData,3000);
}
 
reloadData();

/**
*
*/
function sendMessage()
{
	var url='send_message.php';
		
	var data_form = document.getElementById('message_text_area').value;
	
	if (data_form.length != 0)
	{
		data_form = "user1="+my_id+"&user2="+current_conversation+"&data="+data_form ;
		my_request.abort();	  
  		my_request.open("POST", url, false);
  		my_request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  		my_request.send(data_form); 
  		
  		document.getElementById('message_text_area').value = '';
  	  	
  		changeConversation(my_id, current_conversation);
  	} 	
}

/**
*
*/
function reloadListOfUserOnline(user1, user2)
{
	var url='reload_list_of_user.php?user1='+user1+'&user2='+user2;
	  	  	  	  	  	  
  	my_request.open("GET", url, false);
  	my_request.onreadystatechange=process_data_list;
  	my_request.send(null);
}

/**
*
*/
function process_data_list()
{
  	if (my_request.readyState==4)
  	{
    	if (my_request.status==200)
    	{
      		var my_text = my_request.responseText;
      		document.getElementById("listOfUser").innerHTML = my_text;
    	}
  	}
}

/**
*
*/
function reloadConversation(user1, user2)
{
	var url='change_conversation.php?user1='+user1+'&user2='+user2;
  	  	
  	current_conversation = user2;
  	  	  	  
  	my_request.open("GET", url, false);
  	my_request.onreadystatechange=process_data;
  	my_request.send(null);
}

/**
*
*/
function changeConversation(user1, user2)
{
  	reloadConversation(user1, user2);
  	reloadListOfUserOnline(user1, user2);
}

/**
*
*/
function process_data()
{
  	if (my_request.readyState==4)
  	{
    	if (my_request.status==200)
    	{
      		var my_text = my_request.responseText;
      		document.getElementById("messages").innerHTML = my_text;
    	}
  	}
}

/**
*
*/
function cancel_get_text_data()
{
  	if (my_request!=null)
  	{
    	if ((my_request.readyState>=1) && (my_request.readyState<=3))
    	{
      		my_request.abort();
      		document.getElementById("messages").innerHTML='abandon';
    	}
  	}

}
