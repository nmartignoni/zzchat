<?php

/**
 * zzChatConversation
 *
 * @package ZZCHAT
 * @author	Noel Martignoni
 **/
class zzChatConversation
{
	/**
	 * Constructor for inializing zzChatConversation
	 *
	 * @param   int
	 * @return	null
	 **/
	public function __construct()
	{

	}

	/*
	* Return the conversation between users
	*/
	public static function getConversation($user_1, $user_2)
	{
		if ($user_2 == 0)
		{
			$u1 = 0;
			$u2 = 0;
		}
		else
		{
			if (strval($user_1) > strval($user_2))
			{
				$u1 = $user_2;
				$u2 = $user_1;
			}
			else
			{
				$u1 = $user_1;
				$u2 = $user_2;
			}
		}
		
		zzChatConversation::createConversationIfNotExist($u1, $u2);
		
		$url = 'data/conversation/'.$u1.'_'.$u2.'.xml';					
		
		$content = "";
		
		if (file_exists($url)) 
		{
    		$xml = simplexml_load_file($url);
    		    						
			$content = '<h3>';
			if ($u2 == 0)
				$content .= 'Hello World!';
			else
				$content .= zzChatUser::getUser($user_2)->getLogin();
			$content .= '</h3>';
			
			$message_list_size = 0;
			foreach($xml->message as $message)
			{
				$message_list_size++;
			}	
			
			$message_list_size -= 5;
			
			foreach($xml->message as $message)
			{
			
				if ($message_list_size > 0)
				{
					$message_list_size--;
					continue;
				}
			
				$user = zzChatUser::getUser($message->userid);
				$user_login = $user->getLogin();
				$date = $message->date;
				$message = $message->content;
								
				if ($user->getId() == $user_1)
				{
					$content .= '<div id="talkbubbleRight">'.
					'<div id="message">'.$message.'</div></br>'.
					'<span id="date">'.$date.'</span>'.'</div>';
				}
				else
				{
					$content .=  '<div id="talkbubbleLeft">';
					if ($u2 == 0) $content .= '<span id="user">'.$user_login.'</span></br>';
					$content .= '<div id="message">'.$message.'</div></br>'.
					'<span id="date">'.$date.'</span>'.'</div>';
				}
			}
		}
		else
		{
			/* creer le fichier */
		}
				
		return $content;
	}
	
	/*
	* Create a new conversation
	*
	*/
	public static function createConversationIfNotExist($user_1, $user_2)
	{
		$url = 'data/conversation/';
		
		if (!zzChatConversation::conversationIsCreate($user_1, $user_2))
		{
			$file = fopen($url.'/'.$user_1.'_'.$user_2.'.xml', 'w');	
		
			fputs($file, '<?xml version="1.0" encoding="utf-8"?>');
			fputs($file, '<document>');
			fputs($file, '</document>');
		}		
	}
	
	/**
	* Test if the conversation has been create
	*
	*/
	public static function conversationIsCreate($user_1, $user_2)
	{
		return file_exists('data/conversation/'.$user_1.'_'.$user_2.'.xml');
	}
	
	/**
	* Add a message to the conversation of those users
	*
	*/
	public static function addMessageToConversation($user_1, $user_2, $message)
	{
		if ($user_2 == 0)
		{
			$u1 = 0;
			$u2 = 0;
		}
		else
		{
			if (strval($user_1) > strval($user_2))
			{
				$u1 = $user_2;
				$u2 = $user_1;
			}
			else
			{
				$u1 = $user_1;
				$u2 = $user_2;
			}
		}
		
		$url = 'data/conversation/'.$u1.'_'.$u2.'.xml';
		
		if (file_exists($url)) 
		{
    		$xml = simplexml_load_file($url);
    	
    		$account = $xml->addChild("message");
    		$account->addchild("userid", $user_1);
			$account->addchild("date", date("Y-m-d H:i:s"));
			$account->addChild("content",$message);
 
			$xml->asXml($url);
		}	
	}

}
?>