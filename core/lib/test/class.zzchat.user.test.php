<?php

require_once '../class.zzchat.user.php';

class zzChatUserTest extends PHPUnit_Framework_TestCase
{	
	
	public function testGetNewId()
	{
		$this->assertEquals(2, zzChatUser::getNewId());
	}
	
	public function testLoginIsAvailable()
	{
		$this->assertFalse(zzChatUser::loginIsAvailable('logintest'));
	}
		
	public function testGetUser()
	{
		$user = zzChatUser::getUser(1);
		
		$this->assertEquals(1, $user->getId());
		$this->assertEquals('logintest', $user->getLogin());
		$this->assertEquals('1a1dc91c907325c69271ddf0c944bc72', $user->getPassword());
	}
	
	public function testSearchUser()
	{
		$user = zzChatUser::searchUser('logintest');
		
		$this->assertEquals(1, $user->getId());
		$this->assertEquals('logintest', $user->getLogin());
		$this->assertEquals('1a1dc91c907325c69271ddf0c944bc72', $user->getPassword());
	}
	
	public function testAddUser()
	{
		zzChatUser::addUser('testlogin2', 'pass');
		
		$this->assertXmlFileEqualsXmlFile('/data/users.xml', '/data/users_test.xml');
		
		copy('data/users_src.xml', 'data/users.xml'); 
	}
	
	public function testGetOnlineUser()
	{
		$user_list = zzChatUser::getOnlineUser();
		
		$this->assertEquals(1, $user_list[1]);
	}
	
	public function testAddOnlineUser()
	{
		zzChatUser::addOnlineUser(1);
		
		$this->assertXmlFileEqualsXmlFile('/data/users.xml', '/data/users_test_add.xml');
	}
	
	public function testDeleteOnlineUser()
	{	
		zzChatUser::deleteOnlineUser(1);
		
		$this->assertXmlFileEqualsXmlFile('/data/users.xml', '/data/users_test_del.xml');
		
		copy('data/online_user_src.xml', 'data/online_user.xml'); 
	}
	
}

?>
