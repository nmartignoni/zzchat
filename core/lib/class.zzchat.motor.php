<?php

/**
 * zzChatMotor
 *
 * @package ZZCHAT
 * @author	Noel Martignoni
 * @brief   class used to managing the website
 **/
class zzChatMotor
{

	private $error;

	/**
	 * Construct a new zzChatMotor object
	 **/
	public function __construct()
	{
		$this->error = -1;
	}
	
	/**
	*	Generate the correct page
	*/
	public function createPage()
	{		
		$lang = '1';
		
		if (!isset($_COOKIE['lang']))
		{
			setcookie("lang", 1);
		}
								
		if (isset($_POST))
		{
			if (isset($_POST['submit_sign_up'])) // sign up
    		{      			
      			$this->signUp();
    		}
    		
    		if (isset($_POST['submit_log_in'])) // log in
    		{      		      			
				$this->logIn();
    		}
    		
    		if (isset($_POST['submit_lang'])) // changement de langue
    		{
    			$lang = $this->changeLang();
    		}
    		else
    		{
    			if (isset($_COOKIE['lang']))
				{
					$lang = $_COOKIE['lang'];
				}
    		}
    		
    		if (isset($_POST['submit_log_out'])) // log out
    		{
    			$this->logOut();
    		}
    		
    		if (isset($_POST['submit_send_message'])) // sent message
    		{
    			$this->sendMessage();
    		}
		}
				
		$show = new zzChatShow($lang, $this->error);
		
		$show->createHeaderHTML();
		
		$_SESSION['user_conv'] = 0;

		if (isset($_SESSION['user']))
		{	
			$show->createHomePage();
		}
		else
		{			
			$show->createRegisterAndLoginPage();
		}
	}
	/**
	* Sign Up procedure
	*
	*/
	private function signUp()
	{
		if (strcmp($_POST['login'], '') == 0 || strcmp($_POST['password'], '') == 0)
		{
			$this->error = 1;
		}
		else
		{
			$user_id = zzChatUser::addUser($_POST['login'], $_POST['password']);
		
			if ($user_id != 0) // utilisateur reconnu
      		{
      			$_SESSION['user'] = $user_id;
      			zzChatUser::addOnlineUser($user_id);
      		}
      		else
      		{
      			$this->error = 3;
      		}
		}
		
	}
	
	/**
	* Log In procedure
	*
	*/
	private function logIn()
	{
		if ($_POST['login'] && $_POST['password'])
      	{
      		$user_id = zzChatUser::identifyUser($_POST['login'], $_POST['password']);
      				      		
      		if ($user_id != 0) // utilisateur reconnu
      		{
      			$_SESSION['user'] = $user_id;
      			zzChatUser::addOnlineUser($user_id);
      		}
      		else // identifiant incorrect
      		{
      			$this->error = 2;
      		}
      	}
      	else // champ vide
      	{
      		$this->error = 1;	
      	} 
	}
	
	/**
	* Log out procedure
	*
	*/
	private function logOut()
	{
		if (isset($_SESSION['user']))
		{
			zzChatUser::deleteOnlineUser($_SESSION['user']);
			session_unset();  
 			session_destroy();
 		}
	}
	
	/**
	* change lang procedure
	*
	*/
	private function changeLang()
	{		
		if (isset($_POST['submit_lang']))
		{
			setcookie("lang", $_POST['submit_lang']);
			return $_POST['submit_lang'];
		}
		return 1;
	}
	
	/**
	* Send message procedure
	*
	*/
	private function sendMessage()
	{
		if (isset($_POST['text_message']))
    	{
    		zzChatConversation::addMessage($_SESSION['user'], $_POST['text_message']);
    	}
	}
}
?>