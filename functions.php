<?php
ini_set('display_errors','1'); 
ini_set('display_startup_errors','1'); 
error_reporting (E_ALL);

require_once("core/lib/class.zzchat.user.php");
require_once("core/lib/class.zzchat.show.php");
require_once("core/lib/class.zzchat.conversation.php");
require_once("core/lib/class.zzchat.message.php");
require_once("core/lib/class.zzchat.motor.php");

/*	
* Bibliotheque de fonctions pour site zzchat
* Noel Martignoni
* 01/10/13
*/

/* Variables globales */
$content = getConversation();

/*
* Methode qui retourne un tableau avec le contenu de content.xml
*/
function getConversation()
{
	$url = 'data/conversation.xml';
	$content = "";
		
	if (file_exists($url)) 
	{
    	$xml = simplexml_load_file($url);
				
		$content = '<ul>';
		foreach($xml->message as $message)
		{
			$user = 'user : '.$message->userid;
			$date = 'date : '.$message->date;
			$message = 'message : '.$message->content;
			
			$content = '<li>'.$user.' '.$date.' '.$message.'</li>'.$content;
		}
		$content .= '</ul>';
	}
		
	return $content;
}

/*
* Methode qui ajoute un message au fichier conversation
*
*/
function addMessage()
{
	$url = 'data/conversation.xml';
	$content = "";
		
	if (file_exists($url)) 
	{
    	$xml = simplexml_load_file($url);
    	
    	$account = $xml->addChild("message");
    	$account->addchild("userid","user5");
		$account->addchild("date","18:12");
		$account->addChild("content","Gloup");
 
		$xml->asXml($url);
	}	
}

/*
* Methode qui cree le contenu HTML du header
*
*/
function createHeader()
{		
	$content = "<h1>ZZChat</p>";
			
	return $content;
}

/*
* Methode qui cree le contenu HTML de l'article
*
*/
function createArticle()
{
	$content = "<p> Conversion actuelle </p>".getConversation();
	
	return $content;
}

/*
*
*/
function createSignUpPage()
{
	$zzChatShow = new zzChatShow();
	
	echo '<form action=" " method="post">';
	echo '<label for="login">'.$zzChatShow->lang('L_LOGIN').'</label>';
	echo '<input type="text" id="login" />';
	echo '<label for="password">'.$zzChatShow->lang('L_PASSWORD').'</label>';
	echo '<input type="text" id="password" />';
	echo '</form>';
}

/*
* Methode qui cree le menu HTML de l'article
*
*/
function createMenu()
{
	$content = "<p> Liste conversation </p>";
	
	zzChatUser::getOnlineUser();
	
	return $content;
}

/*
* Methode qui cree le contenu HTML du footer
*
*/
function createFooter()
{
	$content = "<p>Copyright © 2013 Noël Martignoni</p>";
	
	return $content;
}

?>